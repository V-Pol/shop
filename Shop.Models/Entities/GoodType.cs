﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Shop.Models.Entities.Base;

namespace Shop.Models.Entities;

public partial class GoodType : BaseEntity
{
    [StringLength(50)]
    public string TypeName { get; set; } = null!;

    [InverseProperty("GoodType")]
    public virtual ICollection<Good> Goods { get; } = new List<Good>();
}
