﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Shop.Models.Entities.Base;

namespace Shop.Models.Entities;

public partial class Good : BaseEntity
{
    [StringLength(50)]
    public string GoodName { get; set; } = null!;

    public int GoodTypeId { get; set; }

    [Column(TypeName = "money")]
    public decimal Price { get; set; }

    [Column(TypeName = "decimal(10, 2)")]
    public decimal Balance { get; set; }

    [StringLength(10)]
    public string UnitMeasure { get; set; } = null!;

    [ForeignKey("GoodTypeId")]
    [InverseProperty("Goods")]
    public virtual GoodType GoodType { get; set; } = null!;

    [InverseProperty("Good")]
    public virtual ICollection<PurchaseList> PurchaseLists { get; } = new List<PurchaseList>();
}
