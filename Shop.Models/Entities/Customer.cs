﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Shop.Models.Entities.Base;

namespace Shop.Models.Entities;

public partial class Customer : BaseEntity
{
    [Column("FCs")]
    public string Fcs { get; set; } = null!;

    public string? Address { get; set; }

    [StringLength(11)]
    public string? Phone { get; set; }

    [StringLength(50)]
    public string? Email { get; set; }

    [InverseProperty("Customer")]
    public virtual ICollection<Purchase> Purchases { get; } = new List<Purchase>();
}
