﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Shop.Models.Entities.Base;

namespace Shop.Models.Entities;

[Table("PurchaseList")]
public partial class PurchaseList : BaseEntity
{
    public int GoodId { get; set; }

    [Column(TypeName = "decimal(10, 2)")]
    public decimal Count { get; set; }

    [Column(TypeName = "money")]
    public decimal Sum { get; set; }

    public int PurchaseId { get; set; }

    [ForeignKey("GoodId")]
    [InverseProperty("PurchaseLists")]
    public virtual Good Good { get; set; } = null!;

    [ForeignKey("PurchaseId")]
    [InverseProperty("PurchaseLists")]
    public virtual Purchase Purchase { get; set; } = null!;
}
