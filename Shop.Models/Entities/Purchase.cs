﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Shop.Models.Entities.Base;

namespace Shop.Models.Entities;

public partial class Purchase : BaseEntity
{
    public int CustomerId { get; set; }

    [Precision(0)]
    public DateTime PurchaseDate { get; set; }

    [Column(TypeName = "money")]
    public decimal FinalSum { get; set; }

    public int? DeliveryId { get; set; }

    [ForeignKey("CustomerId")]
    [InverseProperty("Purchases")]
    public virtual Customer Customer { get; set; } = null!;

    [ForeignKey("DeliveryId")]
    [InverseProperty("Purchases")]
    public virtual Delivery? Delivery { get; set; }

    [InverseProperty("Purchase")]
    public virtual ICollection<PurchaseList> PurchaseLists { get; } = new List<PurchaseList>();
}
