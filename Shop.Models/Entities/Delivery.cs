﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Shop.Models.Entities.Base;

namespace Shop.Models.Entities;

public partial class Delivery : BaseEntity
{
    [Precision(0)]
    public DateTime DeliveryDate { get; set; }

    [Column(TypeName = "money")]
    public decimal DeliveryPrice { get; set; }

    [InverseProperty("Delivery")]
    public virtual ICollection<Purchase> Purchases { get; } = new List<Purchase>();
}
