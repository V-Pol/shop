﻿using Shop.Dal.EfStructures;
using Shop.ConsoleApp;
using Shop.Dal.Initialization;

ApplicationDbContextFactory factory = new ApplicationDbContextFactory();
ApplicationDbContext context = factory.CreateDbContext(new string[0]);
SampleDataInitializer.InitializeData(context); //для первичной инициализации БД
XMLController xml = new XMLController(context);
xml.LoadFromXML();
Console.ReadLine();