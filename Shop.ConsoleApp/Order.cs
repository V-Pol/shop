﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Shop.ConsoleApp
{
    [Serializable, XmlRoot(ElementName = "orders", DataType = "order")]
    public class Orders
    {
        [XmlElement("order")]
        public Order[]? OrderList { get; set; }
    }
    [Serializable]
    public class Order
    {
        public int no { get; set; }
        public string? reg_date { get; set; }
        public decimal? sum { get; set; }

        public User? user { get; set; }
        [XmlElement("product")]
        public Product[]? products { get; set; }
    }

    public class User
    {
        public string? fio { get; set; }
        public string? email { get; set; }
    }

    public class Product
    {
        public int? quantity { get; set; }
        public string? name { get; set; }
        public decimal? price { get; set; }
    }
}
