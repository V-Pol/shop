﻿using Shop.Dal.EfStructures;
using Shop.Dal.Repos;
using Shop.Models.Entities;
using System.Xml.Serialization;

namespace Shop.ConsoleApp
{
    public class XMLController
    {
        ApplicationDbContext context;
        public XMLController(ApplicationDbContext _context)
        {
            context = _context;
        }
        public void LoadFromXML()
        {
            XmlSerializer formatter = new XmlSerializer(typeof(Orders));
            using (FileStream fs = new FileStream("orders.xml", FileMode.OpenOrCreate))
            {
                if (fs.Length != 0)
                {
                    Console.WriteLine($"Файл {fs.Name} открыт");
                    Orders? orders = formatter.Deserialize(fs) as Orders;
                    if (orders != null)
                    {
                        AddOrdersToDB(orders);
                    }
                    else
                    {
                        Console.WriteLine("Файл не содержит заказов");
                    }
                }
                else
                {
                    Console.WriteLine("Файл не найден");
                }
            }
        }
        #region Добавление заказа в БД
        private void AddOrdersToDB(Orders orders)
        {
            if (orders != null)
            {
                foreach (Order order in orders.OrderList!)
                {
                    PurchaseRepo purchaseRepo = new PurchaseRepo(context);
                    bool orderCheck = purchaseRepo.Find(order.no) == null;
                    if (orderCheck)
                    {
                        Purchase purchase = new Purchase();
                        purchase.Id = order.no;
                        purchase.PurchaseDate = DateTime.ParseExact(order.reg_date!, "yyyy.MM.dd",
                            System.Globalization.CultureInfo.InvariantCulture);
                        purchase.FinalSum = (decimal)order.sum!;
                        if (order.user != null)
                        {
                            if (order.user.fio != null)
                            {
                                Customer customer = GetCustomer(order.user);
                                purchase.Customer = customer;
                                purchaseRepo.Add(purchase);
                            }
                        }
                        if (order.products != null)
                        {
                           AddPurchaseLists(order.products, purchaseRepo.GetLastAddedId());
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Заказ №{order.no} уже присутствует в базе данных");
                    }
                }
            }
        }
        #endregion

        #region Получение из БД или добавление в БД клиента 
        private Customer GetCustomer(User user)
        {
            CustomerRepo customerRepo = new CustomerRepo(context);
            Customer? customer = customerRepo.FindByFCs(user.fio!);
            if (customer == null)
            {
                Console.WriteLine($"Клиент {user.fio} не был найден в базе данных и будет добавлен");
                Customer newCustomer = new Customer();
                newCustomer.Fcs = user.fio!;
                newCustomer.Email = user.email;
                customerRepo.Add(newCustomer);
                customer = newCustomer;
            }
            return customer;
        }
        #endregion

        #region Добавление товаров в список заказа
        private void AddPurchaseLists(Product[] products, int purchaseId)
        {
            PurchaseListRepo purchaseListRepo = new PurchaseListRepo(context);
            GoodRepo goodRepo = new GoodRepo(context);
            foreach (Product product in products)
            {
                if (product.name != null)
                {
                    Good? good = goodRepo.FindByName(product.name);
                    if (good != null)
                    {
                        PurchaseList purchaseList = new PurchaseList();
                        purchaseList.Good = good;
                        purchaseList.Count = (decimal)product.quantity!;
                        purchaseList.Sum = (int)product.quantity * (decimal)product.price!;
                        purchaseList.PurchaseId = purchaseId;
                        purchaseListRepo.Add(purchaseList);
                    }
                    else
                    {
                        Console.WriteLine($"Товар {product.name} не найден");
                    }
                }
            }
            #endregion
        }
    }
}