﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221228120553_Initial')
BEGIN
    CREATE TABLE [Customers] (
        [Id] int NOT NULL IDENTITY,
        [FCs] nvarchar(max) NOT NULL,
        [Address] nvarchar(max) NULL,
        [Phone] nchar(11) NULL,
        [Email] nchar(50) NULL,
        CONSTRAINT [PK_Customers] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221228120553_Initial')
BEGIN
    CREATE TABLE [Deliveries] (
        [Id] int NOT NULL IDENTITY,
        [DeliveryDate] datetime2(0) NOT NULL,
        [DeliveryPrice] money NOT NULL,
        CONSTRAINT [PK_Deliveries] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221228120553_Initial')
BEGIN
    CREATE TABLE [GoodTypes] (
        [Id] int NOT NULL IDENTITY,
        [TypeName] nvarchar(50) NOT NULL,
        CONSTRAINT [PK_GoodTypes] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221228120553_Initial')
BEGIN
    CREATE TABLE [Purchases] (
        [Id] int NOT NULL IDENTITY,
        [CustomerId] int NOT NULL,
        [PurchaseDate] datetime2(0) NOT NULL,
        [FinalSum] money NOT NULL,
        [DeliveryId] int NULL,
        CONSTRAINT [PK_Purchases] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Purchases_Customers] FOREIGN KEY ([CustomerId]) REFERENCES [Customers] ([Id]),
        CONSTRAINT [FK_Purchases_Deliveries] FOREIGN KEY ([DeliveryId]) REFERENCES [Deliveries] ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221228120553_Initial')
BEGIN
    CREATE TABLE [Goods] (
        [Id] int NOT NULL IDENTITY,
        [GoodName] nvarchar(50) NOT NULL,
        [GoodTypeId] int NOT NULL,
        [Price] money NOT NULL,
        [Balance] decimal(10,2) NOT NULL,
        [UnitMeasure] nvarchar(10) NOT NULL,
        CONSTRAINT [PK_Goods] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Goods_GoodTypes] FOREIGN KEY ([GoodTypeId]) REFERENCES [GoodTypes] ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221228120553_Initial')
BEGIN
    CREATE TABLE [PurchaseList] (
        [Id] int NOT NULL IDENTITY,
        [GoodId] int NOT NULL,
        [Count] decimal(10,2) NOT NULL,
        [Sum] money NOT NULL,
        [PurchaseId] int NOT NULL,
        CONSTRAINT [PK_PurchaseList] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_PurchaseList_Goods] FOREIGN KEY ([GoodId]) REFERENCES [Goods] ([Id]),
        CONSTRAINT [FK_PurchaseList_Purchases] FOREIGN KEY ([PurchaseId]) REFERENCES [Purchases] ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221228120553_Initial')
BEGIN
    CREATE INDEX [IX_Goods_GoodTypeId] ON [Goods] ([GoodTypeId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221228120553_Initial')
BEGIN
    CREATE INDEX [IX_PurchaseList_GoodId] ON [PurchaseList] ([GoodId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221228120553_Initial')
BEGIN
    CREATE INDEX [IX_PurchaseList_PurchaseId] ON [PurchaseList] ([PurchaseId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221228120553_Initial')
BEGIN
    CREATE INDEX [IX_Purchases_CustomerId] ON [Purchases] ([CustomerId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221228120553_Initial')
BEGIN
    CREATE INDEX [IX_Purchases_DeliveryId] ON [Purchases] ([DeliveryId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221228120553_Initial')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20221228120553_Initial', N'7.0.1');
END;
GO

COMMIT;
GO

