﻿using Shop.Dal.EfStructures;
using Shop.Models.Entities;
using Shop.Dal.Repos.Base;
using Shop.Dal.Repos.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Shop.Dal.Repos
{
    public class GoodTypeRepo : BaseRepo<GoodType>, IGoodTypeRepo
    {
        public GoodTypeRepo(ApplicationDbContext context) : base(context)
        {
        }

        internal GoodTypeRepo(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
    }
}