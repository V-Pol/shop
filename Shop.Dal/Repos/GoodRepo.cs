﻿using Shop.Dal.EfStructures;
using Shop.Models.Entities;
using Shop.Dal.Repos.Base;
using Shop.Dal.Repos.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Shop.Dal.Repos
{
    public class GoodRepo : BaseRepo<Good>, IGoodRepo
    {
        public GoodRepo(ApplicationDbContext context) : base(context)
        {
        }

        internal GoodRepo(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public Good? FindByName(string Name)
        {
            DbSet<Good> Table = Context.Set<Good>();
            Good? good = Table.FirstOrDefault(g => g.GoodName == Name);
            return good;
        }
    }
}