﻿using Shop.Dal.EfStructures;
using Shop.Models.Entities;
using Shop.Dal.Repos.Base;
using Shop.Dal.Repos.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Shop.Dal.Repos
{
    public class DeliveryRepo : BaseRepo<Delivery>, IDeliveryRepo
    {
        public DeliveryRepo(ApplicationDbContext context) : base(context)
        {
        }

        internal DeliveryRepo(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
    }
}