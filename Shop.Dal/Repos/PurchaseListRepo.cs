﻿using Shop.Dal.EfStructures;
using Shop.Models.Entities;
using Shop.Dal.Repos.Base;
using Shop.Dal.Repos.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Shop.Dal.Repos
{
    public class PurchaseListRepo : BaseRepo<PurchaseList>, IPurchaseListRepo
    {
        public PurchaseListRepo(ApplicationDbContext context) : base(context)
        {
        }

        internal PurchaseListRepo(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
    }
}