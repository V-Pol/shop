﻿using Shop.Dal.EfStructures;
using Shop.Models.Entities;
using Shop.Dal.Repos.Base;
using Shop.Dal.Repos.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Storage;

namespace Shop.Dal.Repos
{
    public class PurchaseRepo : BaseRepo<Purchase>, IPurchaseRepo
    {
        public PurchaseRepo(ApplicationDbContext context) : base(context)
        {
        }

        internal PurchaseRepo(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public override int Add(Purchase entity, bool persist = true)
        {
            using var transaction = Context.Database.BeginTransaction();
            try
            {
                var metaData = Context.Model.FindEntityType(typeof(Purchase).FullName!);
                Context.Database.ExecuteSqlRaw(
                    $"SET IDENTITY_INSERT {metaData!.GetSchema()}.{metaData!.GetTableName()} ON");
                DbSet<Purchase> table = Context.Set<Purchase>();
                table.Add(entity);
                Context.SaveChanges();
                Context.Database.ExecuteSqlRaw(
                    $"SET IDENTITY_INSERT {metaData!.GetSchema()}.{metaData!.GetTableName()} OFF");
                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
            }
            return persist ? 1 : 0;
        }

        public int GetLastAddedId()
        {
            DbSet<Purchase> Table = Context.Set<Purchase>();
            int id = Table.OrderByDescending(p => p.Id)!.FirstOrDefault()!.Id;
            return id;
        }
    }
}