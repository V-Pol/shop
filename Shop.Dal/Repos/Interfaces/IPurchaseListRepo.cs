﻿using Shop.Models.Entities;
using Shop.Dal.Repos.Base;

namespace Shop.Dal.Repos.Interfaces
{
    public interface IPurchaseListRepo : IRepo<PurchaseList>
    {
    }
}