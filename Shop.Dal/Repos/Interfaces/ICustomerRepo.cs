﻿using System.Collections.Generic;
using Shop.Models.Entities;
using Shop.Dal.Repos.Base;

namespace Shop.Dal.Repos.Interfaces
{
    public interface ICustomerRepo : IRepo<Customer>
    {
        Customer? FindByFCs(string FCs);
    }
}
