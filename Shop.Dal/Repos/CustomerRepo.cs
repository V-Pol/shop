﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Shop.Dal.EfStructures;
using Shop.Models.Entities;
using Shop.Dal.Repos.Base;
using Shop.Dal.Repos.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace Shop.Dal.Repos
{
    public class CustomerRepo : BaseRepo<Customer>, ICustomerRepo
    {
        public CustomerRepo(ApplicationDbContext context) : base(context)
        {
        }

        internal CustomerRepo(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public Customer? FindByFCs(string FCs)
        {
            DbSet<Customer> Table = Context.Set<Customer>();
            Customer? customer = Table.FirstOrDefault(c => c.Fcs == FCs);
            return customer;
        }
    }
}
