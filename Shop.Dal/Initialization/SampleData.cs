using System.Collections.Generic;
using Shop.Models.Entities;

namespace Shop.Dal.Initialization
{
    public static class SampleData
    {
        public static List<Customer> Customers => new()
        {
            new() {Id = 1, Fcs = "������ �������", Address = "�������, �����, ��.���������� 51", Phone = "+331123456", Email = "flame@mail.fr"}
        };

        public static List<Delivery> Deliveries => new()
        {
            new() {Id = 1, DeliveryDate = new DateTime(2022,9,15,10,00, 00), DeliveryPrice = 100.0M}
        };
        public static List<GoodType> GoodTypes => new()
        {
            new() {Id = 1, TypeName = "��������"},
            new() {Id = 2, TypeName = "�������"},
            new() {Id = 3, TypeName = "������"}
        };

        public static List<Good> Goods => new()
        {
            new() {Id = 1, GoodName = "LG 1755", GoodTypeId = 1, Price = 12000.75M, Balance = 10, UnitMeasure = "��"},
            new() {Id = 2, GoodName = "Xiomi 12X", GoodTypeId = 1, Price = 42000.75M, Balance = 10, UnitMeasure = "��"},
            new() {Id = 3, GoodName = "Noname 14232", GoodTypeId = 3, Price = 1.7M, Balance = 20, UnitMeasure = "��"},
            new() {Id = 4, GoodName = "Noname 222", GoodTypeId = 3, Price = 3.14M, Balance = 50, UnitMeasure = "��"},
            new() {Id = 5, GoodName = "Noname 1301", GoodTypeId = 3, Price = 2.50M, Balance = 50, UnitMeasure = "��"}
        };

        public static List<Purchase> Purchases => new()
        {
            new() {Id = 2, CustomerId = 1, DeliveryId = 1, PurchaseDate = new DateTime (2022,9,15), FinalSum = 120.0M}
        };

        public static List<PurchaseList> PurchaseLists => new()
        {
            new() {Id = 1, GoodId = 5, Count = 8, PurchaseId = 2, Sum = 20.0M}
        };
    }
}