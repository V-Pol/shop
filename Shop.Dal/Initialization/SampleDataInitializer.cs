using System;
using System.Collections.Generic;
using System.Linq;
using Shop.Dal.EfStructures;
using Shop.Models.Entities;
using Shop.Models.Entities.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace Shop.Dal.Initialization
{
    public static class SampleDataInitializer
    {
        internal static void ClearData(ApplicationDbContext context)
        {
            var entities = new[]
            {
                typeof(Customer).FullName,
                typeof(Delivery).FullName,
                typeof(GoodType).FullName,
                typeof(Good).FullName,
                typeof(Purchase).FullName,
                typeof(PurchaseList).FullName
            };
            foreach (var entityName in entities)
            {
                var entity = context.Model.FindEntityType(entityName!);
                var tableName = entity!.GetTableName();
                var schemaName = entity!.GetSchema();
                context.Database.ExecuteSqlRaw($"DELETE FROM {schemaName}.{tableName}");
                context.Database.ExecuteSqlRaw($"DBCC CHECKIDENT (\"{schemaName}.{tableName}\", RESEED, 1);");
            }
        }

        internal static void SeedData(ApplicationDbContext context)
        {
            try
            {
                ProcessInsert(context, context.Customers!, SampleData.Customers);
                ProcessInsert(context, context.Deliveries!, SampleData.Deliveries);
                ProcessInsert(context, context.GoodTypes!, SampleData.GoodTypes);
                ProcessInsert(context, context.Goods!, SampleData.Goods);
                ProcessInsert(context, context.Purchases!, SampleData.Purchases);
                ProcessInsert(context, context.PurchaseLists!, SampleData.PurchaseLists);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            static void ProcessInsert<TEntity>(
                ApplicationDbContext context, DbSet<TEntity> table, List<TEntity> records) where TEntity : BaseEntity
            {
                if (table.Any())
                {
                    return;
                }

                IExecutionStrategy strategy = context.Database.CreateExecutionStrategy();
                strategy.Execute(() =>
                {
                    using var transaction = context.Database.BeginTransaction();
                    try
                    {
                        var metaData = context.Model.FindEntityType(typeof(TEntity).FullName!);
                        context.Database.ExecuteSqlRaw(
                            $"SET IDENTITY_INSERT {metaData!.GetSchema()}.{metaData!.GetTableName()} ON");
                        table.AddRange(records);
                        context.SaveChanges();
                        context.Database.ExecuteSqlRaw(
                            $"SET IDENTITY_INSERT {metaData!.GetSchema()}.{metaData!.GetTableName()} OFF");
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                    }
                });
            }

        }


        internal static void DropAndCreateDatabase(ApplicationDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.Migrate();
        }

        public static void InitializeData(ApplicationDbContext context)
        {
            DropAndCreateDatabase(context);
            SeedData(context);
        }

        public static void ClearAndReseedDatabase(ApplicationDbContext context)
        {
            ClearData(context);
            SeedData(context);
        }
    }
}