﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Transactions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using Shop.Dal.Exceptions;
using Shop.Models.Entities;
using static System.Collections.Specialized.BitVector32;

namespace Shop.Dal.EfStructures;

public partial class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
        ChangeTracker.StateChanged += ChangeTracker_StateChanged;
        ChangeTracker.Tracked += ChangeTracker_Tracked;

        base.SavingChanges += (sender, args) =>
        {
            Console.WriteLine($"Сохранение изменений для базы данных {((ApplicationDbContext)sender!)!.Database!.GetDbConnection().Database}");
        };

        base.SavedChanges += (sender, args) =>
        {
            Console.WriteLine($"Сохранено {args!.EntitiesSavedCount} изменений");
        };
        base.SaveChangesFailed += (sender, args) =>
        {
            Console.WriteLine($"Произошло исключение! {args!.Exception!.InnerException!.Message}");
        };
    }
    private void ChangeTracker_StateChanged(object? sender, EntityStateChangedEventArgs e)
    {
        string objectEntity = e.Entry!.CurrentValues!.EntityType!.ToString()!.Substring(12);

        var oldState = string.Empty;
        oldState = e.OldState switch
        {
            EntityState.Added => "Добавлен",
            EntityState.Modified => "Отредактирован",
            EntityState.Deleted => "Удалён",
            EntityState.Detached => "Отсоединён",
            EntityState.Unchanged => "Без изменений",
                    _ => oldState
        };
        var newState = string.Empty;
        newState = e.NewState switch
        {
            EntityState.Added => "Добавлен",
            EntityState.Modified => "Отредактирован",
            EntityState.Deleted => "Удалён",
            EntityState.Detached => "Отсоединён",
            EntityState.Unchanged => "Без изменений",
            _ => newState
        };
        if (newState == "Без изменений" )
        {
            if (e.Entry.Entity is Purchase rs)
                Console.WriteLine($"Заказ №{rs.Id} был {oldState}");
            else
                Console.WriteLine($"Объект {objectEntity} был {oldState}");
        }
    }

    private void ChangeTracker_Tracked(object? sender, EntityTrackedEventArgs e)
    {
        var source = (e.FromQuery) ? "База данных" : "Код";
        if (e.Entry.Entity is Purchase rs)
        {
            Console.WriteLine($"Заявка на отправку заказа №{rs.Id} была добавлен из {source}");
        }
    }
    public DbSet<SeriLogEntry>? LogEntries { get; set; }

    public virtual DbSet<Customer>? Customers { get; set; }

    public virtual DbSet<Delivery>? Deliveries { get; set; }

    public virtual DbSet<Good>? Goods { get; set; }

    public virtual DbSet<GoodType>? GoodTypes { get; set; }

    public virtual DbSet<Purchase>? Purchases { get; set; }

    public virtual DbSet<PurchaseList>? PurchaseLists { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<SeriLogEntry>(entity =>
        {
            entity.Property(e => e.Properties).HasColumnType("Xml");
            entity.Property(e => e.TimeStamp).HasDefaultValueSql("GetDate()");
        });

        modelBuilder.Entity<Customer>(entity =>
        {
            entity.Property(e => e.Email).IsFixedLength();
            entity.Property(e => e.Phone).IsFixedLength();
        });

        modelBuilder.Entity<Good>(entity =>
        {
            entity.HasOne(d => d.GoodType).WithMany(p => p.Goods)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Goods_GoodTypes");
        });

        modelBuilder.Entity<Purchase>(entity =>
        {
            entity.HasOne(d => d.Customer).WithMany(p => p.Purchases)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Purchases_Customers");

            entity.HasOne(d => d.Delivery).WithMany(p => p.Purchases).HasConstraintName("FK_Purchases_Deliveries");
        });

        modelBuilder.Entity<PurchaseList>(entity =>
        {
            entity.HasOne(d => d.Good).WithMany(p => p.PurchaseLists)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_PurchaseList_Goods");

            entity.HasOne(d => d.Purchase).WithMany(p => p.PurchaseLists)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_PurchaseList_Purchases");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

    public override int SaveChanges()
    {
        try
        {
            return base.SaveChanges();
        }
        catch (DbUpdateConcurrencyException ex)
        {
            throw new CustomConcurrencyException("Произошла ошибка параллелизма.", ex);
        }
        catch (RetryLimitExceededException ex)
        {
            throw new CustomRetryLimitExceededException("Возникла проблема c SQL Server.", ex);
        }
        catch (DbUpdateException ex)
        {
            throw new CustomDbUpdateException("Произошла ошибка при обновлении базы данных.", ex);
        }
        catch (Exception ex)
        {
            throw new CustomException("Произошла ошибка при обновлении базы данных.", ex);
        }
    }
}
